export function writeResume() {
  return `
  <div class="container">
  <div class="row">
  <div class="col-xl-10 col-lg-9 col-md-8 pt-5">
  
  <div class="wrapper ml-auto" >

  <section id="main">
  <header class="mt-5" id="title">
    <h1 class="lead-2">Karen K</h1>
    <span class="subtitle">Full stack dev</span>
  </header>
  <section class="main-block">
    <h2 class="text-warning">
      <i class="fa fa-suitcase"></i> Experience
    </h2>
    <section>
      <div>
        <span>2018 - present</span>
      </div>
      <div>
        <header>
          <h3>Supreme Being</h3>
          <span>Earth</span>
          <span>(remote-working)</span>
        </header>
        <div>
          <ul>
            <li>
              Changed the gravitational constant of the universe
            </li>
            <li>Transitioned to supreme being</li>
            <li>Declared war on the Tribbles!</li>
          </ul>
        </div>
      </div>
    </section>
    <section>
      <div>
        <span>2010-20180</span>
      </div>
      <div>
        <header>
          <h3>Raised my Children</h3>
          <span>At home</span>
          <span>Vancouver Island, Canada</span>
        </header>
        <div>
          <ul>
            <li>They are still alive!</li>
            <li>They like me sometimes!</li>
            <li>I like them too! (most of the time)</li>
          </ul>
        </div>
      </div>
    </section>
    <section class="blocks">
      <div>
        <span>2013-2014</span>
      </div>
      <div>
        <header>
          <h3>Yet Another Job Position</h3>
          <span>Some Workplace</span>
          <span>Some City, Some Country</span>
        </header>
        <div>
          <ul>
            <li>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Proin nec mi ante. Etiam odio eros, placerat eu metus
              id, gravida eleifend odio
            </li>
            <li>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </li>
          </ul>
        </div>
      </div>
    </section>
  </section>
  <section class="main-block">
    <h2 class="text-warning">
      <i class="fa fa-folder-open"></i> Selected Projects
    </h2>
    <section>
      <div>
        <span>2015-2016</span>
      </div>
      <div>
        <header>
          <h3>Some Project 1</h3>
          <span>Some workplace</span>
        </header>
        <div>
          <ul>
            <li>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </li>
            <li>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Proin nec mi ante. Etiam odio eros, placerat eu metus
              id, gravida eleifend odio. Vestibulum dapibus pharetra
              odio, egestas ullamcorper ipsum congue ac
            </li>
            <li>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Proin nec mi ante. Etiam odio eros, placerat eu metus
              id, gravida eleifend odio
            </li>
          </ul>
        </div>
      </div>
    </section>
  <section>
    <h2 class="text-warning">
      <i class="fa fa-graduation-cap"></i> Education
    </h2>
    <section>
      <div>
        <span>2009-2014</span>
      </div>
      <div>
        <header>
          <h3>Ph.D. in Being Awesome</h3>
          <div>The University</div>
          <div>Somewhere Over There</div>
        </header>
        <div>Discovered the answer to everything</div>
      </div>
    </section>
  </section>
</section>
<br />
<br />
<br />
<br />
<br />
</div>
</div>
</div>
</div>



`;
}
