export function writeFooter() {
  return `<!-- Footer -->
  <footer class="page-footer font-small fixed-bottom dark-color pt-4">
  <div class="container">
  <div class="row">
  <div class="col-12 text-center">
  <p class="h5 text-center text-muted">Karen K - Full Stack Developer Inc. &copy; 2020</p>
  <i class="far fa-envelope fa-2x text-warning mr-5"></i><i class="fab fa-linkedin-in fa-2x text-warning mr-5"></i><i class="fas fa-phone fa-2x text-warning"></i>
  </div>
  </div>
  </div>
  </footer>
`;
}