export function writePortfolio() {
  return `
  <div class="container">

    <div class="wrapper">

      <div class="row">
        <div class="col-xl-10 col-lg-9 col-md-8">
        <br /><br />
        <h1 class="mt-5 lead-2 text-center">My Portfolio</h1>
        </div>
      </div>

      <div class="row text-center">
        <!-- first card -->
        <div class="col-3 mb-5 mr-5">
          <div class="card mt-5" style="width: 18rem;">
          <img src="img/mountain.jpg" class="card-img-top" alt="...">
          <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <a href="#" class="btn btn-warning">Go somewhere</a>
          </div>
          </div>
        </div>
        <!-- second card -->
        <div class="col-3 mb-5 mr-5">
          <div class="card mt-5" style="width: 18rem;">
          <img src="img/at_top.jpg" class="card-img-top" alt="...">
          <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <a href="#" class="btn btn-warning">Go somewhere</a>
          </div>
          </div>
        </div>
        <!-- third card -->
        <div class="col-3 mb-5">
          <div class="card mt-5" style="width: 18rem;">
          <img src="img/Lady_Image.jpg" class="card-img-top image-fluid" alt="...">
          <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <a href="#" class="btn btn-warning">Go somewhere</a>
          </div>
          </div>
        </div>
      </div>
      <!-- end row -->
    </div>
    <!-- end wrapper -->
</div>
<!-- end container -->  

`;
}
